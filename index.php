<?php


$products = [
[
    'name' => 'Мяч',
    'price' => 700
],
[
    'name' => 'Кросовки',
    'price' => 2021
],
[
    'name' => 'Перчатки',
    'price' => 1430
],
[
    'name' => 'Футболка',
    'price' => 550
],
[
    'name' => 'Шорты',
    'price' => 430
],
[
    'name' => 'Баф',
    'price' => 350
],
[
    'name' => 'Штаны',
    'price' => 1200
],
[
    'name' => 'Кофта',
    'price' => 910
],
[
    'name' => 'Гольф',
    'price' => 1000
],
[
    'name' => 'Гетры',
    'price' => 320
]

];

$user_cart = NULL;

if(!empty($_COOKIE['user_cart'])){
    $user_cart = json_decode($_COOKIE['user_cart'], true);
};


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <meta name="Description" content="learning $_COOKIE">
    <title>Let's eat this cookie</title>
</head>
<body>
    <h1>Магазин <strong>"Нужных товаров"</strong> рад приветствовать вас!</h1>
    <div class="container col-md-7">
        <h2>Предлагаемые товары в неограниченном количестве:</h2>
        <table class="table table-info table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название товара</th>
                    <th scope="col">Цена товара</th>
                    <th scope="col">Купить</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($products as $key => $product) : ?>   
                    <tr>
                        <th scope="row"><?= ++$key; ?></th> 
                        <td><?= $product['name']; ?></td>
                        <td><?= $product['price']; ?></td>
                        <td><a href="cart_add.php?buy_item=<?= $key; ?>">Купить</a></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
    </div> 

    
    <div class="container col-md-3 ">
    <?php if(empty($user_cart)): ?>
        <h2>Ваша корзина пуста, скорей наполни её!!!</h2>
        <?php else: ?>
        <h2>В вашей корзине<br>еще много места ;)</h2>

        <table class="table table-danger table-hover">
            <thead>
                <tr>
                    <th scope="col">Название товара</th>
                    <th scope="col">Кол-во товара</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($user_cart as $item => $amount) : ?>   
                    <tr>
                        <td><?=$products[--$item]['name']; ?></td>
                        <td><?=$amount ; ?></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
        <?php endif; ?>
    </div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>      
</body>
</html>